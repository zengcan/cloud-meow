import { defineStore } from 'pinia'

let loginUser = JSON.parse(localStorage.getItem("loginUserInfo"));
export const useMainStore = defineStore('main', {
	state: () => {
		return {
			user: loginUser ? loginUser : null
		}
	},
	actions: {
		login(user) {
			this.user = user;
			localStorage.setItem("loginUserInfo", JSON.stringify(user));
		},
		outLogin() {
			this.user = null;
			localStorage.removeItem("loginUserInfo");
		}
	}
})