import {
	createRouter,
	createWebHashHistory
} from "vue-router"

import { useMainStore } from "../store"

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

const routes = [{
	name: 'Layout',
	path: '/',
	component: () => import("@/layout/index.vue"),
	children: [
		{
			name: 'userData',
			path: '/',
			component: () => import("@/views/data/user.vue")
		},
		{
			name: 'postData',
			path: '/data/post',
			component: () => import("@/views/data/post.vue")
		},
		{
			name: 'discussData',
			path: '/data/discuss',
			component: () => import("@/views/data/discuss.vue")
		},
		{
			name: 'UserList',
			path: '/user/list',
			component: () => import("@/views/user/list.vue")
		},
		{
			name: 'postList',
			path: '/post/list',
			component: () => import("@/views/post/list.vue")
		},
		{
			name: 'discussList',
			path: '/discuss/list',
			component: () => import("@/views/discuss/list.vue")
		},
		{
			name: 'topicList',
			path: '/topic/list',
			component: () => import("@/views/topic/list.vue")
		},
		{
			name: 'topicClass',
			path: '/topic/class',
			component: () => import("@/views/topic/class.vue")
		},
		{
			name: 'setingAdministrator',
			path: '/seting/administrator',
			component: () => import("@/views/seting/administrator.vue")
		}, {
			name: 'setingRole',
			path: '/seting/role',
			component: () => import("@/views/seting/role.vue")
		}, {
			name: 'setingRule',
			path: '/seting/rule',
			component: () => import("@/views/seting/rule.vue")
		}, {
			name: 'setingUpload',
			path: '/seting/upload',
			component: () => import("@/views/seting/upload.vue")
		}, {
			name: 'setingMiniapp',
			path: '/seting/miniapp',
			component: () => import("@/views/seting/miniapp.vue")
		},
		{
			name: 'operateSwiper',
			path: '/operate/swiper',
			component: () => import("@/views/operate/swiper.vue")
		}
	]
},
{
	name: 'Login',
	path: '/user/login',
	component: () => import("@/views/user/login.vue")
}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes,
})


//导航守卫
router.beforeEach((to, from, next) => {
	// 开启进度条
	NProgress.start()

	const mainStore = useMainStore();

	if (!mainStore.user && to.name !== "Login") next({
		name: 'Login'
	})
	else next()
})


router.afterEach(() => {
	// 关闭进度条
	NProgress.done()
})
export default router;
