// yyyy:mm:dd|yyyy:mm|yyyy年mm月dd日|yyyy年mm月dd日 hh时MM分等,可自定义组合
function timeFormat(dateTime = null, fmt = 'yyyy-mm-dd hh:MM') {
	// 如果为null,则格式化当前时间
	if (!dateTime) dateTime = Number(new Date());
	// 如果dateTime长度为10或者13，则为秒和毫秒的时间戳，如果超过13位，则为其他的时间格式
	if (dateTime.toString().length == 10) dateTime *= 1000;
	let date = new Date(Number(dateTime));
	let ret;
	let opt = {
		"y+": date.getFullYear().toString(), // 年
		"m+": (date.getMonth() + 1).toString(), // 月
		"d+": date.getDate().toString(), // 日
		"h+": date.getHours().toString(), // 时
		"M+": date.getMinutes().toString(), // 分
		"s+": date.getSeconds().toString() // 秒
		// 有其他格式化字符需求可以继续添加，必须转化成字符串
	};
	for (let k in opt) {
		ret = new RegExp("(" + k + ")").exec(fmt);
		if (ret) {
			fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
		};
	};
	return fmt;
}

export default {
	timeFormat(row, column) {
		// 获取单元格数据
		let data = row[column.property]

		return timeFormat(data)
	},

	//生成随机字符串
	randomString(len) {
		let _charStr = 'abacdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789', min = 0, max = _charStr.length - 1, _str = '';
		len = len || 32;
		//循环生成字符串
		for (var i = 0, index; i < len; i++) {
			index = (function (randomIndexFunc, i) {
				return randomIndexFunc(min, max, i, randomIndexFunc);
			})(function (min, max, i, _self) {
				let indexTemp = Math.floor(Math.random() * (max - min + 1) + min),
					numStart = _charStr.length - 10;
				if (i == 0 && indexTemp >= numStart) {
					indexTemp = _self(min, max, i, _self);
				}
				return indexTemp;
			}, i);
			_str += _charStr[index];
		}
		return _str;
	}

}
