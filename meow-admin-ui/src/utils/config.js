let domain;

if (process.env.NODE_ENV == 'production') {
	// 生产环境
	domain = "/";
} else {
	// 开发环境
	domain = "http://localhost:3000/";
}

export default {
	domain
};
