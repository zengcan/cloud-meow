import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";

// ElementPlus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import utils from './utils/utils.js';
import { createPinia } from 'pinia';

const app = createApp(App)

app.config.globalProperties.$utils = utils

//全局注册element图标组件
import * as ElIcons from '@element-plus/icons-vue'
for (const name in ElIcons) {
    app.component(name, ElIcons[name])
}
const pinia = createPinia();

app.use(router)
app.use(pinia)
app.use(ElementPlus)
app.mount('#app')
