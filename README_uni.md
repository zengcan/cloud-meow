#### 遵循Apache-2.0开源协议，免费商用，可去除版权信息

#### 系统架构：uniapp+vue3+thinkphp6

#### 介绍

兴趣社区圈子论坛程序,后台服务端基于ThinkPHP6开发，是一套兴趣社区论坛+电商的系统，代码开源免费使用



码云：[点击访问](https://gitee.com/ym721/cloud-meow-uni)

#### 安装教程

1、php版本 >=8.0

2、uniapp端下载后在根目录：utils/config.js中配置站点域名，运行到小程序



#### 公众号
<img src="https://www.meoyun.com/_nuxt/img/wechat.a60e766.png" width = "200px" height = "200px" />

#### 微信联系

<img src="https://www.meoyun.com/kefu.png" width = "200px" height = "200px" />

#### 官网

[云喵服务](https://www.meoyun.com)

#### 定制项目二开请联系微信