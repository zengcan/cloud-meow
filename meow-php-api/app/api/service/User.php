<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\service;

use app\common\model\User as UserModel;
use app\common\service\BaseService;
use think\facade\Db;

class User
{
    /**
     * 创建或查询登录用户信息
     * @param $key
     * @param $value
     * @param $data
     * @return UserModel|array|mixed|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function createOrGetLoginUser($key, $value, $data)
    {
        $user = UserModel::where($key, $value)->find();

        if ($user) {
            $user = $this->userExtendInfo($user);
            $user["token"] = BaseService::buildToken(["userId" => $user["id"]]);
        } else {
            //防止用户名重复
            $username = Db::name("user")->where("username", $data["username"])->value("username");
            if ($username) {
                $data["username"] = $username . rand(1000000, 9999999);
            }

            $data["id"] = BaseService::randomStr();
            $createUser = UserModel::create($data);
            $user = UserModel::find($createUser->id);
            $user = $this->userExtendInfo($user);
            $user["token"] = BaseService::buildToken(["userId" => $user["id"]]);
        }

        UserModel::update(["id" => $user["id"], "last_login_ip" => request()->ip()]);

        return $user;

    }

    /**
     * 用户扩展信息计算
     * @param $user
     * @return mixed
     */
    public function userExtendInfo($user): mixed
    {
        //用户等级
        $exp = $user["exp"];
        $user["level"] = $this->userLevel($exp);

        //是否关注
        $sessionUid = BaseService::getLoginUserIdNoAuth();
        $user["is_follow"] = false;

        if ($sessionUid) {
            $isFollow = Db::name("follow")->where(["user_id" => $sessionUid, "follow_uid" => $user["id"]])->find();
            if ($isFollow) {
                $user["is_follow"] = true;
            }
        }

        return $user;
    }

    /**
     * 计算用户等级
     * @param $exp
     * @return string
     */
    public function userLevel($exp): string
    {

        $level = 'Lv.1';

        // Lv.1
        if ($exp < 1000) {
            $level = 'Lv.1';
        }

        // Lv.2
        if ($exp >= 1000 && $exp < 3000) {
            $level = 'Lv.2';
        }

        // Lv.3
        if ($exp >= 3000 && $exp < 5000) {
            $level = 'Lv.3';
        }

        // Lv.4
        if ($exp >= 5000 && $exp < 7000) {
            $level = 'Lv.4';
        }

        // Lv.5
        if ($exp >= 7000 && $exp < 9000) {
            $level = 'Lv.5';
        }

        // Lv.6
        if ($exp >= 9000 && $exp < 12000) {
            $level = 'Lv.6';
        }

        // Lv.7
        if ($exp >= 12000 && $exp < 16000) {
            $level = 'Lv.7';
        }

        // Lv.8
        if ($exp >= 16000 && $exp < 50000) {
            $level = 'Lv.8';
        }

        // Lv.9
        if ($exp >= 50000 && $exp < 100000) {
            $level = 'Lv.9';
        }

        // Lv.10
        if ($exp >= 100000) {
            $level = 'Lv.10';
        }

        return $level;
    }
}