<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\service;

use think\facade\Db;
use app\common\model\Topic as TopicModel;

/**
 * 圈子服务类
 */
class Topic
{

    /**
     * 获取用户加入的圈子列表
     * @param $userId
     * @param $paginate
     * @return \think\Paginator
     */
    public function getUserTopic($userId, $paginate = 10)
    {
        $topicIds = Db::name("user_topic")->where("user_id", $userId)->column("topic_id");
        $where[] = ["topic.id", "in", $topicIds];
        $list = TopicModel::getList($where, "id desc", $paginate);

        return $list;
    }
}