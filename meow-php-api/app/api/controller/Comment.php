<?php

namespace app\api\controller;

use app\common\service\BaseService;
use app\common\service\Message;
use app\common\service\Wechat;
use think\facade\Db;
use app\common\model\Comment as CommentModel;

class Comment extends ApiBase
{

    //评论列表
    public function list()
    {
        $postId = input("post_id");
        $where["pid"] = 0;
        $uid = BaseService::getLoginUserIdNoAuth();
        if (!empty($postId)) {
            $where["post_id"] = $postId;
        }

        $list = CommentModel::withJoin(["userInfo"])->order("id desc")->where($where)->paginate(10)->each(function ($item) use ($uid) {

            $item["thumb_num"] = Db::name("comment_thumb")->where("c_id", $item["id"])->count();

            $item["is_thumb"] = false;
            $isCon = Db::name("comment_thumb")->where(["user_id" => $uid, "c_id" => $item["id"]])->find();
            if ($isCon) {
                $item["is_thumb"] = true;
            }

            $children = CommentModel::withJoin(["userInfo"])->where("pid", $item["id"])->select()->each(function ($item2) use ($uid) {
                $item2["to_user"] = Db::name("user")->where("id", $item2["to_uid"])->find();
                $item2["thumb_num"] = Db::name("comment_thumb")->where("c_id", $item2["id"])->count();

                $item2["is_thumb"] = false;
                $isCon = Db::name("comment_thumb")->where(["user_id" => $uid, "c_id" => $item2["id"]])->find();
                if ($isCon) {
                    $item2["is_thumb"] = true;
                }

            });

            if (count($children) > 0) {
                $item["children"] = $children;

            } else {
                $item["children"] = [];
            }

            return $item;

        });

        return message($list);
    }

    //评论点赞
    public function thumbAdd()
    {
        $id = input("id");

        $userId = BaseService::getLoginUserId();

        $data["id"] = BaseService::randomStr();
        $data["c_id"] = $id;
        $data["user_id"] = $userId;
        $isCon = Db::name("comment_thumb")->where($data)->find();
        if ($isCon) {
            return error("请勿重复点赞");
        }

        $data["create_time"] = time();

        $res = Db::name("comment_thumb")->insert($data);

        if ($res) {
            $comment = CommentModel::find($id);
            Message::send($userId, $comment["user_id"], $comment["post_id"], 1, "赞了您的评论：" . $comment["content"]);
            return message("点赞成功");
        }

        return error("点赞失败");
    }

    //取消点赞
    public function cancelThumb()
    {
        $id = input("id");
        $userId = BaseService::getLoginUserId();

        $where["c_id"] = $id;
        $where["user_id"] =$userId;

        $res = Db::name("comment_thumb")->where($where)->delete();
        if ($res) {
            return message("已取消点赞");
        }

        return error("取消失败");
    }

    //删除评论
    public function del()
    {
        $id = input("id");

        $userId = BaseService::getLoginUserId();
        $commentInfo = CommentModel::where("id", $id)->find();
        $res = CommentModel::destroy($id);
        if ($res) {
            Db::name("post")->where("id", $commentInfo["post_id"])->dec('comment_num')->update();
            return message("删除成功");
        }
        return error("删除失败");
    }

    //发表评论
    public function addComment()
    {

        $userId = BaseService::getLoginUserId();
        $data = input("post.");
        $data["user_id"] = $userId;
        $data["id"] = BaseService::randomStr();

        $user = Db::name("user")->find($userId);

        if ($user["status"] != 0) {
            return error("该用户已被禁用");
        }

        //微信小程序用户检测内容安全
        if ($user["type"] == 1) {
            $wechat = new Wechat();

            if ($data["content"] && !$wechat->checkText($data["content"], $user["openid"])) {
                return error("内容违规违法");
            }
        }

        $res = \app\common\model\Comment::create($data);

        if ($res) {
            $msgToUid = input("to_uid");

            if (empty($msgToUid)) {
                $msgToUid = Db::name("post")->where("id", $data["post_id"])->value("user_id");
            }

            Db::name("post")->where("id", $data["post_id"])->inc('comment_num')->update();
            Message::send($userId, $msgToUid, $data["post_id"], 2, "评论：" . $data["content"]);
            return message($res, "评论成功");
        }

        return error("评论失败");
    }
}
