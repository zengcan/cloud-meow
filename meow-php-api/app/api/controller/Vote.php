<?php


namespace app\api\controller;


use app\common\model\VoteResult;
use app\common\service\BaseService;
use think\facade\Db;

class Vote extends ApiBase
{

    //用户投票
    public function userVote(){
        $id = input("id");
        $vote = input("vote");

        $userId = BaseService::getLoginUserId();
        $res = VoteResult::create(["id"=>BaseService::randomStr(),"vote_id" => $id,"result" => $vote,"user_id" =>$userId]);
        if ($res){

            foreach ($vote as $item){
                Db::name("vote_option")->where("id",$item)->inc('ticket_num')->update();
            }

            return message("投票成功");
        }

        return error("投票失败");
    }
}