<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;

use think\facade\Db;
use app\common\service\BaseService;
use app\common\model\Message as MessageModel;

class Message extends ApiBase
{
    public function list()
    {
        $msgType = input("type");
        $userId = BaseService::getLoginUserIdNoAuth();
        $where[] = ["message.status", "in", [0, 1]];

        if ($msgType == 1) {
            $where[] = ["to_uid", "=", $userId];
            $where[] = ["message.type", "in", [1, 3]];
        }

        if ($msgType == 3) {
            $where[] = ["to_uid", "=", $userId];
            $where[] = ["message.type", "=", 2];
        }
        if ($msgType == 5) {
            $where[] = ["message.type", "=", 5];
            $list = MessageModel::withJoin(["userInfo" => ["username", "avatar"]])->order("create_time desc")->where($where)->select();
        } else if ($msgType == 6) {
            $where[] = ["message.type", "=", 6];
            $list = MessageModel::withJoin(["userInfo" => ["username", "avatar"]])->order("create_time desc")->where($where)->select();
        } else if ($msgType != 2) {
            $list = MessageModel::withJoin(["userInfo" => ["username", "avatar"], "postInfo"])->order("create_time desc")->where($where)->select();
        } else {
            $where[] = ["to_uid", "=", $userId];
            $where[] = ["message.type", "=", 4];
            $list = MessageModel::withJoin(["userInfo" => ["username", "avatar"]])->order("create_time desc")->where($where)->select();
        }
        return message($list);
    }

    public function status()
    {
        $msgType = input("type");

        $uid = BaseService::getLoginUserId();

        $where[] = ["to_uid", "=", $uid];

        if ($msgType == 1) {
            $where[] = ["type", "in", [1, 3]];
        }

        if ($msgType == 2) {
            $where[] = ["type", "=", 4];
        }

        if ($msgType == 3) {
            $where[] = ["type", "=", 2];
        }
        if ($msgType == 5) {
            $where[] = ["type", "=", 5];
        }
        if ($msgType == 6) {
            $where[] = ["type", "=", 6];
        }

        $res = Db::name("message")->where($where)->update(["status" => 1]);

        if ($res) {
            return message("消息状态修改成功");
        }
        return error("消息状态修改失败");
    }

    public function num()
    {

        $userId = BaseService::getLoginUserIdNoAuth();
        $where["status"] = 0;

        $thumbCollect = Db::name("message")->where("to_uid", $userId)->where($where)->where("type", "in", [1, 3])->count();
        $follow = Db::name("message")->where("to_uid", $userId)->where($where)->where("type", 4)->count();
        $comment = Db::name("message")->where("to_uid", $userId)->where($where)->where("type", 2)->count();
        $allCount = Db::name("message")->where("to_uid", $userId)->where($where)->count();

        //推送文章消息
        $article_msg_list = Db::name("message")->where("to_uid", $userId)->where("type", 5)->order("create_time desc")->select()->each(function ($item) {
            $item["article_title"] = Db::name("post")->where("id", $item["post_id"])->value("title");
            $item["user_info"] = Db::name("user")->where("uid", $item["from_uid"])->find();
            return $item;
        });

        //私信
        $chatUids = Db::name("message")->where("to_uid", $userId)->where("type", 6)->order("create_time desc")->column("from_uid");

        $chatUids = array_unique($chatUids);

        $chatMsgList = [];
        $where1["to_uid"] = $userId;
        foreach ($chatUids as $key => $id) {
            $where1["from_uid"] = $id;

            $msg["count"] = Db::name("message")->where($where1)->where("type", 6)->where("status", 0)->count();
            $msg["msg"] = Db::name("message")->where($where1)->order("create_time desc")->limit(1)->find();
            $msg["user_info"] = Db::name("user")->where("uid", $id)->find();

            array_push($chatMsgList, $msg);
        }

        $data = [
            "thumb_collect" => $thumbCollect,
            "follow" => $follow,
            "comment" => $comment,
            "all_count" => $allCount,
            "article_msg_list" => $article_msg_list,
            "chat_msg_list" => $chatMsgList
        ];

        return message($data);
    }
}