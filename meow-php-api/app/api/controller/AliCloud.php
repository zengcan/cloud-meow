<?php

namespace app\api\controller;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use app\common\model\System as SystemModel;

class AliCloud extends ApiBase
{

    //调用STS服务AssumeRole
    public function getAuth()
    {
        $aliyunInfo = SystemModel::where("key", "aliyun_upload")->find();

        //构建一个阿里云客户端，用于发起请求。
        //设置调用者（RAM用户或RAM角色）的AccessKey ID和AccessKey Secret。
        $aliyun = $aliyunInfo->value;
        AlibabaCloud::accessKeyClient($aliyun["AccessKeyId"],$aliyun["AccessKeySecret"])
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Sts')
                ->scheme('https') // https | http
                ->version('2015-04-01')
                ->action('AssumeRole')
                ->method('POST')
                ->host('sts.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' =>$aliyun["RegionId"],
                        'RoleArn' =>$aliyun["RoleArn"],
                        'RoleSessionName' =>$aliyun["RoleSessionName"]
                    ],
                ])
                ->request();

            return message(array_merge($result->toArray(),$aliyun));
        } catch (ClientException $e) {
            return error($e->getErrorMessage());
        } catch (ServerException $e) {
            return error($e->getErrorMessage());
        }
    }
}