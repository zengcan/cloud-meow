<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;

use app\common\model\Link as LinkModel;

/**
 * 链接类
 */
class Link extends ApiBase
{
    public function list()
    {
        $type = input("type");
        $list = LinkModel::getList($type);
        return message($list);
    }
}