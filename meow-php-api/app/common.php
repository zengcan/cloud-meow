<?php
// 应用公共文件

use think\facade\Env;
use think\response\Json;

/**
 * 当前是否为调试模式
 * @return bool
 */
function is_debug(): bool
{
    return (bool)Env::instance()->get('APP_DEBUG');
}

/**
 * 统一返回
 * @param array $data
 * @param string $msg
 * @param int $code
 * @return Json
 */
function message($result = [], string $msg = "SUCCESS", int $code = 200): Json
{

    $data = [
        "code" => $code,
        "msg" => $msg
    ];

    if (is_string($result)) {
        $data["msg"] = $result;
    } else {
        $data["result"] = $result;
    }


    return json($data, $code);
}


/**
 * 错误统一返回
 * @param string $msg
 * @param int $code
 * @return Json
 */
function error(string $msg = "系统发生错误，请稍后再试！", int $code = 500, array $result = []): Json
{

    $data = [
        "code" => $code,
        "msg" => $msg,
        "result" => $result,
    ];

    return json($data);
}