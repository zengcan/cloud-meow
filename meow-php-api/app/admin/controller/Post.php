<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\admin\controller;

/**
 * 帖子控制器
 */

use app\common\model\Post as PostModel;

class Post extends AdminBase
{
    public function delete()
    {
        $userIds = input("post_ids/a");

        $res = PostModel::destroy($userIds);

        if($res){
            return message('删除成功');
        }

        return error("删除失败");
    }
}