<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\admin\controller;

use app\common\model\System as SystemModel;

class System extends AdminBase
{

    public function info()
    {
        $keyword = input("keyword");
        $info = SystemModel::where("key", $keyword)->find();

        return message($info->value);
    }

    public function save()
    {
        $data = input("post.");

        $res = SystemModel::update(["value" => $data],["key" => $data["key"]]);

        if($res){
            return message("保存成功");
        }

        return  error("保存失败");
    }
}