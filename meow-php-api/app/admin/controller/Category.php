<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\admin\controller;

use app\common\model\Category as CategoryModel;
class Category extends AdminBase
{
    public function delete()
    {
        $userIds = input("class_ids/a");

        $res = CategoryModel::destroy($userIds);

        if($res){
            return message('删除成功');
        }

        return error("删除失败");
    }
}