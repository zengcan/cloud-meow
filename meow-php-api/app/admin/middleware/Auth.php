<?php

namespace app\admin\middleware;

use app\common\model\Rule;
use app\common\service\BaseService;
use think\facade\Db;
use think\facade\Request;

/**
 * 后台权限中间件
 */

class Auth {
    public function handle($request, \Closure $next) {

        $token = Request::header("Authorization");

        $userId = BaseService::getLoginUserId($token);

        $groupId = Db::name("admin")->where("id",$userId)->value("group_id");

        $ruleIds = Db::name("group_rule")->where("group_id",$groupId)->column("rule_id");

        $ruleUrl = Db::name("rule")->where("is_menu",0)->where("id","in",$ruleIds)->column("url");

        $appName =  app('http')->getName();
        $pathinfo = $request->pathinfo();
        $rule = $appName .'/' .$pathinfo;

        if(!in_array($rule,$ruleUrl)){
            abort(500,"没有访问权限");
        };

        return $next($request);
    }
}
