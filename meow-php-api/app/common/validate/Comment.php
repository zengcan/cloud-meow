<?php

namespace app\common\validate;

use think\Validate;

class Comment extends Validate
{
    protected $rule =   [
        'uid'  => 'require',
        'to_uid' => 'require',
        'comment_id' => 'require',
        'reply_type' => 'require',
        // 'pid' => 'number',
        'content'   => 'require|max:255',
        'post_id' => 'require'
    ];

    protected $message  =   [
        'uid.number' => '非法uid',
        'pid.number' => '非法pid',
        'content.require' => '内容不能为空',
        'to_uid.require' => '通知对象不能为空',
        'uid.require' => '评论作者不能为空',
        'from_uid.require' => '回复作者不能为空',
        'reply_type.require' => '回复类型不能为空',
        'post_id.require' => '评论对象ID不能为空',
        'comment_id.require' => '评论ID不能为空',
        'reply_id.require' => '回复对象id不能为空',
        'content.max' => '内容不能超过255字符',
    ];

    protected $scene  =   [
        'comment' => ['uid', 'content', 'post_id', 'to_uid'],
        'add_reply' => ['from_uid', 'content', 'comment_id', 'reply_type', 'reply_id'],
        'post_comment' => ['post_id']
    ];
}
