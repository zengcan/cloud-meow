<?php

namespace app\common\validate;

use think\Validate;

class Admin extends Validate
{
    protected $rule =   [
        'username'  => 'require|min:4|max:25|unique:admin',
        'password'   => 'require|min:6',
    ];

    protected $message  =   [
        'username.require' => '用户名不能为空',
        'username.max'     => '用户名最多不能超过25个字符',
        'username.min'     => '用户名最少4个字符',
        'username.unique' => '用户名已存在',
        'password.require'   => '请填写密码',
        'password.min'     => '密码最少6个字符'
    ];

}