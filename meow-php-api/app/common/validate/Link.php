<?php

namespace app\common\validate;

use think\Validate;

class Link extends Validate
{
    protected $rule =   [
        'title'  => 'require',
        'img'   => 'require',
    ];

    protected $message  =   [
        'title.require' => '标题必填',
        'img.require' => '请上传图片'
    ];

}