<?php


namespace app\common\model;


use think\Model;

class VoteResult extends Model
{

    protected $json = ['result'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;
}