<?php

namespace app\common\model;

use think\Model;

class Discuss extends Model
{

    public function userInfo()
    {
        return $this->hasOne("User", "id", "user_id");
    }

    public static function getList($where = "", $order = "id desc", $paginate = 10)
    {
        $list = self::withJoin(['userInfo' => ['username', 'avatar']])->where($where)->order($order)->paginate($paginate);
        return $list;
    }
}