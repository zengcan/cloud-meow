<?php


namespace app\common\model;


use think\Model;
use think\facade\Db;

class Topic extends Model
{

    public function userInfo()
    {
        return $this->hasOne("User", "id", "user_id");
    }

    public static function getList($where = "", $order = "create_time desc", $paginate = 10)
    {
        $list = self::withJoin(["userInfo"])->where($where)->order($order)->paginate($paginate);
        return $list;
    }
}