<?php


namespace app\common\model;


use think\Model;

class Comment extends Model
{
    public function userInfo()
    {
        return $this->hasOne("User", "id", "user_id");
    }

    public function childrenList()
    {
        return $this->hasMany(CommentReply::class, 'comment_id', 'id');
    }
}
