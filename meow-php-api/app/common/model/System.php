<?php


namespace app\common\model;


use think\Model;

class System extends Model
{

    protected $pk = "key";

    protected $json = ['value'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;
}