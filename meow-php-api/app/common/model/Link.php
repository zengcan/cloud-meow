<?php

namespace app\common\model;


use think\Model;

class Link extends Model
{
    protected $autoWriteTimestamp = true;
    
    public static function getList($type = "",$order = "id desc",$paginate = 10)
    {
        $list = self::where("type",$type)->order($order)->paginate($paginate);
        return $list;
    }
}