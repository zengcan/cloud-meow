<?php

namespace app\common\model;

use think\Model;

class User extends Model
{
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;

    protected $json = ['tag_str'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    public function getGenderAttr($value)
    {
        $status = [0=>'未知',1=>'男',2=>'女'];
        return $status[$value];
    }
    public static function getList($where = "", $order = "create_time desc", $paginate = 10)
    {
        $list = self::where($where)->order($order)->paginate($paginate);
        return $list;
    }
}
