<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

class Utils
{

    /**
     * 重组树形结构数据
     * @param $arr
     * @param $level
     * @param $id
     * @return array
     */
    public static function makeTree($arr,$level = 0,$id = 0)
    {
        $list =array();
        foreach ($arr as $k=>$v){
            if ($v['pid'] == $id){
                $v['level']=$level;
                $v['children'] = self::makeTree($arr,$level+1,$v['id']);
                $list[] = $v;
            }
        }
        return $list;
    }

    /**
     * @description: 获取一个日期范围内的日期
     * @param {interval:日期范围,type：取值类型，-：获取之前日期；+：获取之后的日期}
     * @return:
     */
    public static function getDateInterval(int $interval, string $type): array
    {
        $dateArr = [];
        for ($i = $interval - 1; $i >= 0; $i--) {
            array_push($dateArr, date('Y-m-d', strtotime("{$type}{$i} day")));
        }
        if ($type == '+') $dateArr = array_reverse($dateArr);
        return $dateArr;
    }
}