<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

use think\facade\Db;
use app\common\model\Topic as TopicModel;

class Topic
{
    /**
     * 根据用户ID获取加入的圈子列表
     * @param $userId
     * @return \think\Paginator
     */
    public static function getJoinListByUserId($userId): \think\Paginator
    {
        $topicIds = Db::name("user_topic")->where("uid", $userId)->column("topic_id");
        $where[] = ["topic.id", "in", $topicIds];
        return TopicModel::getList($where, "id desc", 100);
    }
}