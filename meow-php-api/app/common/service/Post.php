<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------

namespace app\common\service;

use app\common\model\Post as PostModel;

class Post
{

    /**
     * 根据用户ID获取发布帖子列表
     * @param array $userIds
     * @return \think\Paginator
     */
    public static function getPostListByUserId(array $userIds)
    {
        $where[] = array("post.user_id", "in", $userIds);
        $list = PostModel::getList($where);

        return $list;
    }
}