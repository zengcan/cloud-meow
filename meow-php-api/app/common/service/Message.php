<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

/**
 * 消息服务类
 */

use think\facade\Db;

class Message
{

    public static function send($from_uid, $to_uid, $post_id, $type, $content = "", $title = "")
    {

        $userId = BaseService::getLoginUserId();
        /**
         * type 1为点赞，2为评论  3为收藏 4为关注  5为推送文章 6私聊
         */

        $data["id"] = BaseService::randomStr();
        $data["from_uid"] = $from_uid;
        $data["to_uid"] = $to_uid;
        $data["post_id"] = $post_id;
        $data["title"] = $title;
        $data["content"] = $content;
        $data["type"] = $type;
        $data["create_time"] = time();

        $where["from_uid"] = $from_uid;
        $where["to_uid"] = $to_uid;
        $where["type"] = $type;
        $where["post_id"] = $post_id;
        $where["content"] = $content;

        if ($from_uid != $to_uid) {
            $res = Db::name("message")->where($where)->find();
            if (!$res) {

                //点赞获得经验与积分
                if ($type == 1 || $type == 3) {
                    Db::name('user')->where('id', $userId)->inc('exp', 1)->update();
                    Db::name('user')->where('id', $userId)->inc('integral', 1)->update();
                }

                //评论获得经验与积分
                if ($type == 2) {
                    Db::name('user')->where('id', $userId)->inc('exp', 5)->update();
                    Db::name('user')->where('id', $userId)->inc('integral', 5)->update();
                }

                Db::name("message")->insert($data);
            }
        }
    }
}