<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

use EasyWeChat\MiniApp\Application;
use app\common\model\System as SystemModel;

class Wechat
{
    public $MiniApp = null;

    public function __construct()
    {

        //微信小程序配置
        $miniAppInfo = SystemModel::where("key", "miniapp")->find();

        $wechatConfig = $miniAppInfo->value;
        $config = [
            'app_id' => $wechatConfig["appid"],
            'secret' => $wechatConfig["secret"]
        ];

        $this->MiniApp = new Application($config);
    }

    //小程序文本内容检测
    public function checkText($content, $openid): bool
    {
        $api = $this->MiniApp->getClient();

        $response = $api->postJson('/wxa/msg_sec_check', [
            'content' => $content,
            'version' => 2,
            'scene' => 3,
            'openid' => $openid
        ]);

        if ($response["errcode"] === 0) {
            return true;
        }

        return false;
    }

    //微信小程序校验图片/音频是否含有违法违规内容
    public function mediaCheck($mediaUrl, $openid, $mediaType = 2)
    {
        $api = $this->MiniApp->getClient();

        $response = $api->postJson('/wxa/media_check_async', [
            'media_url' => $mediaUrl,
            'media_type' => $mediaType, //1:音频;2:图片
            'version' => 2,
            'scene' => 3,
            'openid' => $openid,
        ]);

        if ($response["errcode"] === 0) {
            return true;
        }

        return false;
    }

}